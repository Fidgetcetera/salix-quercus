# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Fidgetcetera

# THANKS

    /* All the people who help you build this! */

# TECHNOLOGY COLOPHON

    CSS3, HTML5
    Apache Server Configs, jQuery, Modernizr, Normalize.css
